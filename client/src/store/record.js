'use strict';
const { AuthService } = require( "../../../server/services/auth.service" )
export default {
    state: {
        records: []
    },
    mutations: {
        set_records(state, payload){
            state.records = payload
        }
    },
    actions: {
        async CREATE_RECORD( { commit, dispatch }, record ) {
            try {
                const { getters: { info } } = await dispatch( 'GET_UID' )
                console.log('CREATE_RECORD tt - ', info);
                const data = {
                    userId: info.userId,
                    walletId: info._id,
                    ...record
                }
                console.log('CREATE_RECORD - ',data);
                await AuthService.createRecord( data )
            } catch ( e ) {
                commit( 'set_error' )
                throw e
            }

        },

        async FETCH_RECORDS({dispatch, commit}){
            try {
                const categories = await AuthService.getCategories()
                const records  = await AuthService.getRecord()
                commit('set_records', records.data)
                return [categories.data, records.data]
            }catch ( e ) {
                commit( 'set_error', e )
                throw  e
            }
        },
        async GET_RECORD_BYID({commit}, payload) {
            try {
                const r = await AuthService.getRecordById( payload )
                return  r.data
            }catch ( e ) {
                commit('set_error', e)
            }
        }
    },
}

