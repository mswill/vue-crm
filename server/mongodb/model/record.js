'use strict';
const { Schema, model } = require( 'mongoose' )

const RecordSchema = new Schema( {
    title: {
      type: String,
      required: true
    },
    type: {
       type: String,
       required: true
    },
    amount: {
        type: Number,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    categoryId: {
        type: Schema.Types.ObjectId,
        ref: 'Catetory',
    }
} )

module.exports = model( 'Record', RecordSchema, 'record' )


