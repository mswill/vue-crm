'use strict';

const  {api}   = require( './api.config' )

export class AuthService {

    static async createUser( user ) {
        return api().post( `/auth/user/create`, user )
    }

    static async loginUser(user) {
        return api().post('/auth/user/login', user)
    }
    static async wallet(userId){
        return api().post('/auth/user/wallet', userId)
    }
    static async getWallet(){
        return api().get('/auth/user/getwallet')
    }
    static async createCategory(data){
        return api().post( `/auth/user/category`, data )
    }
    static async getCategories(){
        return api().get( `/auth/user/category` )
    }
    static async getRecord(){
        return api().get( `/auth/user/records` )
    }
    static async getRecordById(id){
        return api().get( `/auth/user/record/${id}` )
    }
    static async updateCategory(data){
        return api().put( `/auth/user/category/:id`, data )
    }
    static async createRecord(record){
        return api().post( `/auth/user/record`, record )
    }
}
