'use strict';
const { Schema, model } = require( 'mongoose' )

const CatetorySchema = new Schema( {
    title: {
        type: String,
        required: true
    },
    limit: {
        type: Number,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
} )

module.exports = model( 'Catetory', CatetorySchema, 'category' )


