"use strict"
import Vue from "vue"
import Vuex from "vuex"
import user from "./user.store"
import info from "./info"
import category from "./category.store"
import record from "./record"

Vue.use( Vuex )


export default new Vuex.Store( {
    state: {
        error: null
    },
    mutations: {
        set_error( state, error ) {
            state.error = error
        },
        clear_error( state ) {
            state.error = null
        }
    },
    actions: {
        async FETCH_CURRENCY() {
            const key = process.env.VUE_APP_FIXER
            const res = await fetch( `http://data.fixer.io/api/latest?access_key=${ key }&symbols=USD,EUR,RUB` )
            return await res.json()
        }
    },
    getters: {
        error: state => state.error
    },
    modules: {
        user,
        info,
        category,
        record
    }
} )
