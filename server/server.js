'use strict';
const mongoose = require( 'mongoose' )
const { Promise } = require( 'bluebird' )
const app = require( './app.js' )
const config = require( './keys' )
const pid = process.pid

const PORT = process.env.PORT || 8081

process.env.NODE_ENV = 'production'
const startServer = () => {
    app.listen( PORT )
    console.log( `Server is work` );
    console.log( `master started. PID=${ pid }` );
    // console.log( 'NODE_ENV ', process.env.NODE_ENV );
}

const startMongoDB = () => {
    mongoose.Promise = Promise
    mongoose.connect( config.dbPath, config.dbOption )
    // console.log( 'MongoDB is work' );
    return mongoose.connection
}

startMongoDB()
    .on( 'error', console.log )
    .on( 'disconnected', startMongoDB )
    .on( 'open', startServer )

