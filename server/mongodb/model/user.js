'use strict';
const { Schema, model } = require( 'mongoose' )

const UserSchema = new Schema( {
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    walletId: {
        type: Schema.Types.ObjectId,
        ref: 'Info',
    }
} )

module.exports = model( 'User', UserSchema, 'users' )
