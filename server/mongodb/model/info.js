'use strict';
const { Schema, model } = require( 'mongoose' )

const InfoSchema = new Schema( {
    wallet: {
        type: Number,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
} )

module.exports = model( 'Info', InfoSchema, 'info' )


