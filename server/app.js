'use strict';
const express = require( 'express' )
const compression = require('compression')
const morgan = require( 'morgan' );
const cors = require('cors')
// const cors = require('cors')
// const multer = require( 'multer' )

const app = express()

// Middleware
app.use( compression() )
app.use(cors())
// app.use(cors({  credentials: true }))
app.use(express.urlencoded({ extended: true}));
app.use( express.json() );

// app.set( 'view engine', 'pug' );
// app.set( 'views', path.resolve( process.cwd(), 'src', 'template' ) );
// app.use(express.static(__dirname));


const authRoute = require( './routes/auth.route' )
app.use( morgan( 'dev' ) )



app.use( '/auth', authRoute )



module.exports = app;

