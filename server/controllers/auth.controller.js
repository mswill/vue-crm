'use strict';
const bcrypt = require( 'bcrypt' )
const createError = require( 'http-errors' )
const User = require( '../mongodb/model/user' )
const Info = require( '../mongodb/model/info' )
const Category = require( '../mongodb/model/category' )
const Record = require( '../mongodb/model/record' )

module.exports.userCreate = async ( req, res ) => {
    try {
        // console.log( 'server side userCreate ---', req.body );
        const { email, password, name, info } = req.body
        const candidate = await User.findOne( { email: email } )

        if ( candidate ) {
            res.json( { message: 'Такой email уже существует' } )
        } else {
            const passwordHash = await bcrypt.hash( password, 12 )
            const user = new User( { email, password: passwordHash, name, info } )
            await user.save()
            res.status( 201 ).json( user )
        }

    } catch ( e ) {
        res.status( 500 ).json( { message: e } )
    }
}

module.exports.userLogin = async ( req, res ) => {
    try {
        //console.log( 'server side userLogin ---', req.body );
        const { email, password } = req.body
        const candidate = await User.findOne( { email: email } ).populate( 'walletId', 'wallet' )

        if ( candidate ) {
            const passCompare = await bcrypt.compare( password, candidate.password )

            if ( passCompare ) {
                res.json( { user: candidate, message: 'Вы вошли в CRM' } )
            } else {
                res.json( { message: 'Пароли несовпадают' } )
            }
        } else {
            res.json( { message: 'Такого email не существет' } )
        }
    } catch ( e ) {
        res.status( 500 ).json( { message: 'ОООШИБКА!!!!' } )
    }
}

module.exports.wallet = async ( req, res ) => {
    try {
        const { userId } = req.body
        //console.log( req.body );
        const candidate = await Info.findOne( { userId: userId } )

        if ( candidate ) {
            res.json( { message: 'Кошелек уже есть!' } )
        } else {
            const wallet = new Info( {
                wallet: 0,
                userId: userId
            } )
            const walletId = await wallet.save()
            await User.findOneAndUpdate( { _id: userId }, { walletId: walletId }, { new: true } )
        }
        res.send( 'ok' )

    } catch ( e ) {
        res.status( 500 ).json( { message: e } )
    }

}

module.exports.getWallet = async ( req, res ) => {
    try {
        const wallets = await Info.find( {} )
        res.json( wallets )
    } catch ( e ) {
    }
}

module.exports.category = async ( req, res ) => {
    try {
        const candidate = await Category.findOne( { _id: req.body._id } )

        if ( candidate ) {
            res.json( { message: 'Обновить категорию' } )
        } else {
            const { _id, title, limit } = req.body;
            const newCaterogyData = new Category( { userId: _id, title, limit } )
            await newCaterogyData.save()
            res.status( 201 ).json( 'new caterory created' )
        }

    } catch ( e ) {
        res.status( 500 ).json( { message: 'ОООШИБКА!!!!' } )
    }
}

module.exports.categories = async ( req, res ) => {
    try {
        const categoriesData = await Category.find( {} )
        res.json( categoriesData )
    } catch ( e ) {
        res.status( 404 ).json( { message: 'Категории не нашлись' } )
    }
}

module.exports.updateCategory = async ( req, res ) => {
    try {
        const { id, title, limit } = req.body
        const data = await Category.findOneAndUpdate( { _id: id }, { title, limit } )
        res.status( 204 ).json( data )
    } catch ( e ) {
        res.status( 404 ).json( { messege: 'Не получилось обновить category' } )
    }
}

module.exports.createRecord = async ( req, res ) => {
    try {

        const record = new Record( { ...req.body } )
        await record.save()

        if ( req.body.type === 'income' ) {
            const wallet = await Info.findOne( { _id: req.body.walletId } )
            const updateWallet = await wallet.wallet + req.body.amount
            await Info.updateOne( { _id: req.body.walletId }, { wallet: updateWallet } )
        } else {
            const wallet = await Info.findOne( { _id: req.body.walletId } )
            const updateWallet = await wallet.wallet - req.body.amount
            await Info.updateOne( { _id: req.body.walletId }, { wallet: updateWallet } )
        }

        await record.save()
        res.status( 201 ).json( { ...req.body } )
    } catch ( e ) {
        res.status( 500 ).json( 'Не получилось сохранить record' )
    }
}

module.exports.getRecords = async ( req, res ) => {
    try {
        const records = await Record.find( {} )
        res.json( records )
    } catch ( e ) {
        res.status( 500 ).json( { message: 'records ошибка' } )
    }
}
module.exports.getRecordById = async ( req, res ) => {
    try {
        const {id} = req.params
        const data = await Record.findOne( { _id: id } )
        res.json( data )
    } catch ( e ) {
        res.status(500).json({ message: 'record by id not found'} )
    }
}
