"use strict";
const { AuthService } = require( "../../../server/services/auth.service" )

export default {
    state: {
        user: null,
    },
    mutations: {
        // create_user( state, payload ) {
        //     state.user = payload
        // },
        login_user( state, payload ) {
            state.user = payload
        }
    },
    actions: {
        async CREATE_USER( { commit, dispatch }, payload ) {
            try {
                const user = await AuthService.createUser( payload )
                if ( user.data.message === 'Такой email уже существует' ) {
                    throw new Error()
                }
                const data = user.data
                dispatch( 'WALLET', { userId: data._id } )
                dispatch( 'GET_UID', data._id )
                // commit( "create_user", payload )
            } catch ( e ) {
                commit( 'set_error', e )
                throw  e
            }
        },
        async GET_UID( uId ) {
            // console.log( 'uid ', uId );
            return uId
        },
        async WALLET( { commit }, userId ) {
            await AuthService.wallet( userId )
        },
        async LOGIN_USER( { commit, dispatch }, payload ) {
            try {
                const user = await AuthService.loginUser( payload )
                console.log('user -- ',user.data.user);
                if ( user.data.message === 'Пароли несовпадают' || user.data.message === 'Такого email не существет' ) {
                    commit( 'set_error', { message: user.data.message } )
                    throw new Error()
                }
                // dispatch( 'FETCH_INFO', user.data.user.walletId )
                dispatch( 'FETCH_INFO', user.data.user )
                commit( 'login_user', user.data.user )
            } catch ( e ) {
                throw e
            }
        },
        async LOGOUT( { commit } ) {
            try {
                commit( 'clear_info' )
            } catch ( e ) {
                throw e
            }
        }
    },
    getters: {
        getUser( state ) {
            return state.user
        }
    }
}
