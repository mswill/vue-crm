'use strict';
const axios = require( 'axios' )

module.exports.api = () => {
    return axios.create( {
        baseURL: 'http://localhost:8081'
    } )
}
