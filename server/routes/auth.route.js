'use strict';
const { Router } = require( 'express' )
const router = Router()

const { userCreate, userLogin, wallet, category, categories, updateCategory, createRecord, getWallet,  getRecords,getRecordById } = require( '../controllers/auth.controller' )


router.post( '/user/create', userCreate )
router.post( '/user/login', userLogin )
router.post( '/user/wallet', wallet )
router.get('/user/getwallet', getWallet)
router.post( '/user/category', category )

router.get( '/user/category', categories )
router.put( '/user/category/:id', updateCategory )
router.post( '/user/record', createRecord )
router.get( '/user/records', getRecords )
router.get('/user/record/:id', getRecordById)
module.exports = router


