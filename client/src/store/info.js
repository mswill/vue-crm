'use strict';
const { AuthService } = require( "../../../server/services/auth.service" )
export default {
    state: {
        info: {}
    },
    mutations: {
        set_info(state, info){
            state.info = info
        },
        clear_info(state){
            console.log('clear_info!');
            state.info = {}
        }
    },
    actions: {
        async FETCH_INFO( { commit }, info ) {
           try {
               const data = await AuthService.getWallet()
               const tt = await data.data.find( user => user.userId === info._id )
               console.log('tt -- ',tt);
               console.log('info --', info )
               commit('set_info', tt)
           }catch ( e ) {
               throw e
           }
        }
    },
    getters: {
        info: state => state.info
    }
}
