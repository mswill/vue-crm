import Vue from 'vue'
import Vuelidate from 'vuelidate'

import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from "./filters/date.filter"
import currencyFilter from './filters/currency.filter'
import Paginate from 'vuejs-paginate'
import MessagePlugin from './utils/message.plugin'
import Loader from './components/app/Loader'
import TooltipDir from './directives/tooltip.directive'

import './registerServiceWorker'
import "materialize-css/dist/css/materialize.min.css";
import "./assets/index.css";
import 'materialize-css/dist/js/materialize.min'

Vue.use( Vuelidate )
Vue.use( MessagePlugin )
Vue.directive('tooltip', TooltipDir)
Vue.filter( 'date', dateFilter )
Vue.filter( 'currency', currencyFilter )
Vue.component('Paginate', Paginate)

//делаем Loader глобальным
Vue.component('Loader', Loader)
Vue.config.productionTip = false


new Vue( {
    router,
    store,
    render: h => h( App )
} ).$mount( '#app' )
