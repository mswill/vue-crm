'use strict';

export default {
    // eslint-disable-next-line no-unused-vars
    install( Vue, options ) {
        Vue.prototype.$myMessage = function ( html ) {
            // eslint-disable-next-line no-undef
            M.toast( { html } )
        }

        Vue.prototype.$myError = function (html) {
            // eslint-disable-next-line no-undef
            M.toast({html:  `[Ошибка]: ${html}`})
        }
    }
}
