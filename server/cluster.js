'use strict';
const os = require( 'os' )
const cluster = require( 'cluster' )
const pid = process.pid

if ( cluster.isMaster ) {
    const cpusCount = os.cpus().length
    console.log( `cpus= ${ cpusCount }` );
    console.log( `master started. PID= ${ pid }` );

    for ( let i = 0; i < cpusCount - 1; i++ ) {
        const worker = cluster.fork()
        cluster.schedulingPolicy = cluster.SCHED_NONE
        console.log('schedulingPolicy, -', cluster.schedulingPolicy);
        worker.on( 'exit', () => {
            cluster.fork()
            console.log( `worker is die Pid${ worker.process.pid }` );
        } )
        worker.send( 'Hello form server!' )
    }
}
if ( cluster.isWorker ) {
    require( './server' )
    process.on('message', (msg)=>{
        console.log('Message from master ', msg);
    })
}


