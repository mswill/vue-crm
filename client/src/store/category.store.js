'use strict';
const { AuthService } = require( "../../../server/services/auth.service" )

export default {
    actions: {
        async CREATE_CATEGORY( { commit, dispatch }, { title, limit } ) {
            try {
                // забираем $store -> getters -> info -> ( _id )
                const { getters: { info: { _id } } } = await dispatch( 'GET_UID' )
                await AuthService.createCategory( { _id, title, limit } )
                console.log( { _id, title, limit } );
                return { _id, title, limit }
            } catch ( e ) {
                commit( 'set_error', e )
                throw e
            }
        },
        async FETCH_CATEGORIES( { commit } ) {
            try {
                // забираем $store -> getters -> info -> ( _id )
                const categories = await AuthService.getCategories()
                console.log('FETCH_CATEGORIES ', categories.data );
                return categories.data
            } catch ( e ) {
                commit( 'set_error', e )
                throw e
            }
        },
        async UPDATE_CATEGOTY( {}, data ) {
            await AuthService.updateCategory( data )
        }
    }
}

